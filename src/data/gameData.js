export default {
  players: ['black', 'white'],
  mainPlayer: 'white',
  table: {
    white: {
      0: 0, 1: 1, 2: 2, 3: 3,
      4: 4, 5: 5, 6: 6, 7: 7,
      8: 8, 9: 9, 10: 10, 11: 11,
    },
    black: {
      0: 28, 1: 29, 2: 30, 3: 31,
      4: 24, 5: 25, 6: 26, 7: 27,
      8: 20, 9: 21, 10: 22, 11: 23,
    },
  },
  baseRect: {
    white: [
      [95, 95], [223, 95], [351, 95], [479, 95],
      [159, 159], [287, 159], [415, 159], [543, 159],
      [95, 223], [223, 223], [351, 223], [479, 223],
    ],
    black: [
      [159, 415], [287, 415], [415, 415], [541, 415],
      [95, 479], [223, 479], [351, 479], [479, 479],
      [159, 543], [287, 543], [415, 543], [543, 543]
    ]
  },
  zoneRect: [
    [95, 95], [223, 95], [351, 95], [479, 95],
    [159, 159], [287, 159], [415, 159], [543, 159],
    [95, 223], [223, 223], [351, 223], [479, 223],
    [159, 287], [287, 287], [415, 287], [543, 287],
    [95, 351], [223, 351], [351, 351], [479, 351],
    [159, 415], [287, 415], [415, 415], [541, 415],
    [95, 479], [223, 479], [351, 479], [479, 479],
    [159, 543], [287, 543], [415, 543], [543, 543],
  ],
  chipMoves: {
    0: {
      down: {
        left: null,
        right: 4
      },
      up: {
        left: null,
        right: null
      }
    },
    1: {
      down: {
        left: 4,
        right: 5
      },
      up: {
        left: null,
        right: null
      }
    },
    2: {
      down: {
        left: 5,
        right: 6
      },
      up: {
        left: null,
        right: null
      }
    },
    3: {
      down: {
        left: 6,
        right: 7
      },
      up: {
        left: null,
        right: null
      }
    },
    4: {
      down: {
        left: 8,
        right: 9
      },
      up: {
        left: 0,
        right: 1
      }
    },
    5: {
      down: {
        left: 9,
        right: 10
      },
      up: {
        left: 1,
        right: 2
      }
    },
    6: {
      down: {
        left: 10,
        right: 11
      },
      up: {
        left: 2,
        right: 3
      }
    },
    7: {
      down: {
        left: 11,
        right: null
      },
      up: {
        left: 3,
        right: null
      }
    },
    8: {
      down: {
        left: null,
        right: 12
      },
      up: {
        left: null,
        right: 4
      }
    },
    9: {
      down: {
        left: 12,
        right: 13
      },
      up: {
        left: 4,
        right: 5
      }
    },
    10: {
      down: {
        left: 13,
        right: 14
      },
      up: {
        left: 5,
        right: 6
      }
    },
    11: {
      down: {
        left: 14,
        right: 15
      },
      up: {
        left: 6,
        right: 7
      }
    },
    12: {
      down: {
        left: 16,
        right: 17
      },
      up: {
        left: 8,
        right: 9
      }
    },
    13: {
      down: {
        left: 17,
        right: 18
      },
      up: {
        left: 9,
        right: 10
      }
    },
    14: {
      down: {
        left: 18,
        right: 19
      },
      up: {
        left: 10,
        right: 11
      }
    },
    15: {
      down: {
        left: 19,
        right: null
      },
      up: {
        left: 11,
        right: null
      }
    },
    16: {
      down: {
        left: null,
        right: 20
      },
      up: {
        left: null,
        right: 12
      }
    },
    17: {
      down: {
        left: 20,
        right: 21
      },
      up: {
        left: 12,
        right: 13
      }
    },
    18: {
      down: {
        left: 21,
        right: 22
      },
      up: {
        left: 13,
        right: 14
      }
    },
    19: {
      down: {
        left: 22,
        right: 23
      },
      up: {
        left: 14,
        right: 15
      }
    },
    20: {
      down: {
        left: 24,
        right: 25
      },
      up: {
        left: 16,
        right: 17
      }
    },
    21: {
      down: {
        left: 25,
        right: 26
      },
      up: {
        left: 17,
        right: 18
      }
    },
    22: {
      down: {
        left: 26,
        right: 27
      },
      up: {
        left: 18,
        right: 19
      }
    },
    23: {
      down: {
        left: 27,
        right: null
      },
      up: {
        left: 19,
        right: null
      }
    },
    24: {
      down: {
        left: null,
        right: 28
      },
      up: {
        left: null,
        right: 20
      }
    },
    25: {
      down: {
        left: 28,
        right: 29
      },
      up: {
        left: 20,
        right: 21
      }
    },
    26: {
      down: {
        left: 29,
        right: 30
      },
      up: {
        left: 21,
        right: 22
      }
    },
    27: {
      down: {
        left: 30,
        right: 31
      },
      up: {
        left: 22,
        right: 23
      }
    },
    28: {
      down: {
        left: null,
        right: null
      },
      up: {
        left: 24,
        right: 25
      }
    },
    29: {
      down: {
        left: null,
        right: null
      },
      up: {
        left: 25,
        right: 26
      }
    },
    30: {
      down: {
        left: null,
        right: null
      },
      up: {
        left: 26,
        right: 27
      }
    },
    31: {
      down: {
        left: null,
        right: null
      },
      up: {
        left: 27,
        right: null
      }
    },
  }
}