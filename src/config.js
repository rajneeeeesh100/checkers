import Boot from './scenes/Boot';
import Game from './scenes/Game';

export default {
  width: 690,
  height: 690,
  backgroundColor: 0x000000,
  type: Phaser.AUTO,
  scene: [ Boot, Game ]
}
