import SpriteController from '../util/SpriteController';

export default class Dice extends SpriteController {
  constructor(name, scene, color) {
    super(name, scene);
    this.name = name;
    this.scene = scene;
    this.color = color;
    this.key = 'dice_rolling';

    this.state = {
      animationStart: false,
      point: 0,
      dice: null
    };

    this.create();
  }

  create() {
    this.createDice(this.color);
    this.dice.setInteractive({ cursor: 'pointer' }).on('pointerdown', this.diceOnClick.bind(this));
  }

  getRandom() {
    return (Math.round(Math.random() * 10) % 6) + 1;
  }

  createDice(color) {
    const diceNum = this.getRandom();
    const rect = this.getDiceRect(color);
    this.dice = this.scene.add.sprite(rect[0], rect[1], "ludo", `dice_${diceNum}`);
    this.dice.fixedToCamera = true;
    this.dice.setOrigin(rect[2], rect[3]);
    this.text = this.scene.add.text(rect[0], rect[4], 'Roll...', { color: 'white', fontFamily: 'Arial', stroke: 0x000000, strokeThickness: 5 });
    this.text.setOrigin(rect[2], rect[3]);
    this.dice.alpha = 0;
    this.text.alpha = 0;
  }

  getDiceRect(color) {
    let rect = [0, 0, 0, 0];
    if (color === 'blue') {
      rect = [69, 621, 0.5, 0.5, 661];
    } else if (color === 'red') {
      rect = [69, 69, 0.5, 0.5, 29];
    } else if (color === 'green') {
      rect = [ 621, 69, 0.5, 0.5, 29];
    } else {
      rect = [ 621, 621, 0.5, 0.5, 661];
    }

    return rect;
  }

  setDiceAnim(dice) {
    const frames = [];
    let num = 0;
    for (let i = 0; i < 10; i++) {
      const diceNum = this.getRandom();
      const name = `dice_${diceNum}`;
      num = diceNum;
      frames.push(name)
    };
    const key = this.key;

    if (this.scene.anims.exists(key)) {
      this.scene.anims.remove(key);
    }

    this.scene.anims.create({
      key,
      frames: this.scene.anims.generateFrameNames('ludo', {
        frames
      }),
      frameRate: 16,
      repeat: 0
    });
    
    this.changeState('dice', null);
    this.scene.eventNumber = 2;
    dice.play(key)
    this.state.animationStart = true;
    this.resetOpacity()
    dice.on('animationcomplete', () => {
      this.resetOpacity()
      this.state.animationStart = false;
      this.state.point += 1;
      this.changeState('dice', num);
      dice.angle = 0;
      setTimeout(() => this.scene.eventNumber = 3, 200);
    })
  }

  resetOpacity() {
    this.dice.alpha = 1;
    this.text.alpha = 1;
  }

  diceOnClick (pointer, object) {
    if (this.scene.eventNumber !== 1) {
      return false;
    }
    if (!this.state.animationStart) {
      this.setDiceAnim(this.dice);
    }
  }

  update(time, delta) {
    const { dice, text } = this;
    const { animationStart } = this.state;
    const { eventNumber } = this.scene;

    if (eventNumber === 1) {
      if (text.text !== 'Roll...') {
        text.text = 'Roll...';
      }

      if (dice.cursor !== 'pointer') {
        dice.input.cursor = 'pointer'
      }

      const sec = Math.round(time / 50);
      if (sec !== this.time) {
        this.time = sec;
        this.spriteHideAnim(text, animationStart);
        if (!animationStart) {
          this.spriteHideAnim(dice, animationStart);
        }
      }
    }

    if (eventNumber === 2) {
      this.rotateSprite(this.dice, 18.93, animationStart);
      if (text.alpha !== 0) {
        text.alpha = 0
      }

      if (dice.cursor !== 'default') {
        dice.input.cursor = 'default'
      }
    }

    if (eventNumber === 3) {
      if (dice.cursor !== 'default') {
        dice.input.cursor = 'default'
      }

      const sec = Math.round(time / 50);
      if (sec !== this.time) {
        this.time = sec;
        this.spriteHideAnim(text, animationStart);
      }
    }

    if (eventNumber === 4 || eventNumber === 5) {
      if (text.text !== 'Go...') {
        text.text = 'Go...';
      }

      const sec = Math.round(time / 50);
      if (sec !== this.time) {
        this.time = sec;
        this.spriteHideAnim(text, animationStart);
      }
    }
  }
}