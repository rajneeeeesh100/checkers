export default class SpriteController {
  constructor(name, scene) {
    this.name = name;
    this.scene = scene;
  }

  getRandom() {
    return (Math.round(Math.random() * 10) % 6) + 1;
  }

  rotateSprite(sprite, angle, condition) {
    if (condition) {
      sprite.angle += angle;
    }
  }

  update(time, delta) {
    
  }

  spriteHideAnim(sprite, condition) {
    if (!condition) {
      sprite.visible = true;
    } else {
      sprite.visible = false;
    }
    if (sprite.alpha === 1) {
      sprite.alpha = 0.96;
    } else if (sprite.alpha === 0.96) {
      sprite.alpha = 0.91;
    } else if (sprite.alpha === 0.91) {
      sprite.alpha = 0.81;
    } else if (sprite.alpha === 0.81) {
      sprite.alpha = 0.71;
    } else if (sprite.alpha === 0.71) {
      sprite.alpha = 0.61;
    } else if (sprite.alpha === 0.61) {
      sprite.alpha = 0.51;
    } else if (sprite.alpha === 0.51) {
      sprite.alpha = 0.41;
    } else if (sprite.alpha === 0.41) {
      sprite.alpha = 0.31;
    } else if (sprite.alpha === 0.31) {
      sprite.alpha = 0.25;
    } else if (sprite.alpha === 0.25) {
      sprite.alpha = 0.2;
    } else if (sprite.alpha === 0.2) {
      sprite.alpha = 0.3;
    } else if (sprite.alpha === 0.3) {
      sprite.alpha = 0.4;
    } else if (sprite.alpha === 0.4) {
      sprite.alpha = 0.5;
    } else if (sprite.alpha === 0.5) {
      sprite.alpha = 0.6;
    } else if (sprite.alpha === 0.6) {
      sprite.alpha = 0.7;
    } else if (sprite.alpha === 0.7) {
      sprite.alpha = 0.8;
    } else if (sprite.alpha === 0.8) {
      sprite.alpha = 0.85;
    } else if (sprite.alpha === 0.85) {
      sprite.alpha = 0.9;
    } else if (sprite.alpha === 0.9) {
      sprite.alpha = 0.95;
    } else {
      sprite.alpha = 1;
    } 
  }

  changeState(key, value) {
    this.scene.state[key] = value;
  }

  destroy(scene) {
    const name = this.name;
    this.dice.destroy();
    this.text.destroy();
    for (let key in this) {
      delete this[key];
    };
    delete scene[name];
  }
}