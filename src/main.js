import config from './config';

const startPhaser = () => {
  const game = new Phaser.Game(config);
};

window.onload = startPhaser;