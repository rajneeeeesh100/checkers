import gameData from '../data/gameData';

export default class Game extends Phaser.Scene {
  constructor(){
    super('Game');
    this.eventNumber = 0;
    this.chips = [];
    this.players = gameData.players;
    this.mainPlayer = gameData.mainPlayer;
    this.baseRect = gameData.baseRect;
    this.zoneRect = gameData.zoneRect;
    this.currentPlayer = gameData.mainPlayer;
    this.chipMoves = gameData.chipMoves;
    this.chipClicked = null;
    this.chipClickedIndex = null;
    this.shadedChipClicked = null;
    this.repeatMove = false;
    this.state = {
      animationStart: false,
      currentPlayer: gameData.mainPlayer,
      table: gameData.table
    }
  }

  create() {
    window.thisR = this;
    const {players} = this;
    this.background = this.add.sprite(0, 0, "images", "grid_8.png");
    this.highlight = this.add.sprite(0, 0, "images", "highlight.png")
      .setDisplaySize(90, 90)
      .setDepth(3)
      .setData({name: 'default'})
      .setAlpha(0);

    this.lightWhite1 = this.add.sprite(0, 0, "images", 'white_pawn.png')
      .setDisplaySize(50, 50)
      .setAlpha(0)
      .setInteractive({ cursor: 'pointer' });

    this.lightWhite2 = this.add.sprite(0, 0, "images", 'white_pawn.png')
      .setDisplaySize(50, 50)
      .setAlpha(0)
      .setInteractive({ cursor: 'pointer' });

    this.lightBlack1 = this.add.sprite(0, 0, "images", 'black_pawn.png')
      .setDisplaySize(50, 50)
      .setAlpha(0)
      .setInteractive({ cursor: 'pointer' });

    this.lightBlack2 = this.add.sprite(0, 0, "images", 'black_pawn.png')
      .setDisplaySize(50, 50)
      .setAlpha(0)
      .setInteractive({ cursor: 'pointer' });

    this.lightWhite1.on('pointerdown', () => {
      this.shadedChipClicked = this.lightWhite1;
    });
    this.lightWhite2.on('pointerdown', () => {
      this.shadedChipClicked = this.lightWhite2;
    });
    this.lightBlack1.on('pointerdown', () => {
      this.shadedChipClicked = this.lightBlack1;
    });
    this.lightBlack2.on('pointerdown', () => {
      this.shadedChipClicked = this.lightBlack2;
    });

    for (let i = 0; i < 12; i++) {
      players.forEach(color => this.createFigure(color, i));
    }
  }

  update(time, delta) {
    switch(this.eventNumber) {
      case 1: {
        //Chip clicked
        this.updateTable();
        this.onChipClicked();
        break;
      }
      case 2: {
        //Showing light shaded chips
        this.showValidMoves();
        break;
      }
      case 3: {
        //Move the chip
        this.moveChip();
        break;
      }
      case 4: {
        //Move the chip
        this.changeTurn();
        break;
      }
      default: {
        //Idle
        this.hideMoves();
      }
    }
  }

  getPosByIndex(color, index, onStart = true, curIndex) {
    const chipPos = (onStart) ? this.state.table[color][index] : curIndex;

    let rect = [];

    if (typeof chipPos === 'number' && chipPos <= 31) {
      rect = this.zoneRect[chipPos];
    } else {
      console.log('Out of board boundaries');
    }

    return rect;
  }

  createFigure(color, i) {
    let direction = color === 'white' ? 'down' : 'up';
    const file = color + '_' + i;
    const filename = color + '_pawn.png';
    const rect = this.getPosByIndex(color, i);
    let thisRef = this;
    let newChip = this.add.sprite(rect[0], rect[1], "images", filename)
      .setDisplaySize(50, 50)
      .setInteractive({ cursor: 'pointer' })
      .setData({name: file, color: color, direction: direction});
    this.chips.push(newChip);
    newChip.inputEnabled=true;
    newChip.on('pointerdown', (e) => {
      this.zoneRect.forEach(function(item, index) {
        if(item[0] === newChip.x && item[1] === newChip.y) {
          thisRef.chipClicked = newChip;
          thisRef.chipClickedIndex = index;
          thisRef.eventNumber = 1;
        }
      })
    });
  }

  updateTable() {
    this.chips = this.chips.filter(chip => chip.getData('name'));
    this.chips.forEach(chip => {
      this.zoneRect.forEach((item, index) => {
        if(item[0] === chip.x && item[1] === chip.y) {
          try {
            let chipNumber = chip.getData('name').split('_')[1];
            let chipColor = chip.getData('color');
            this.state.table[chipColor][chipNumber] = parseInt(index);
          }
          catch (e) {
            console.log(chip, index);
          }
        }
      })
    })
  }

  changeTurn() {
    this.currentPlayer = this.currentPlayer === 'black' ? 'white' : 'black';
    this.eventNumber = 0;
  }

  onChipClicked() {
    if(!this.chipClicked || this.chipClickedIndex < 0) {
      this.eventNumber = 0;
      return false;
    }
    if(this.chipClicked.getData('color') !== this.currentPlayer) {
      return false;
    }
    this.hideMoves();
    if(this.highlight.getData('name') === this.chipClicked.getData('name')) {
      this.chipClicked = null;
      this.chipClickedIndex = null;
      this.highlight.setData({name: 'default'}).setAlpha(0);
      return false;
    }
    this.highlight.setPosition(this.chipClicked.x - 20, this.chipClicked.y - 20)
      .setAlpha(1)
      .setData({name: this.chipClicked.getData('name')});
    this.eventNumber = 2;
  }

  showValidMoves(isRepeatMove) {
    let color = this.chipClicked.getData('color');
    let direction = this.chipClicked.getData('direction');
    let indexData = direction !== 'both' ? this.chipMoves[this.chipClickedIndex][direction] : this.chipMoves[this.chipClickedIndex];
    indexData.color = color;
    indexData.direction = direction;
    let canRemove = false;

    if(direction === 'both') {
      //King pawn
    }
    else {
      //Normal pawn
      if(indexData.left !== null) {
        const moveData = this.isIndexEmpty(indexData.left);
        if(moveData.status) {
          this.showShaded(indexData.left, color);
        }
        else {
          canRemove = true;
          this.showOpponentRemove(indexData);
        }
      }

      if(indexData.right !== null) {
        const moveData = this.isIndexEmpty(indexData.right);
        if(moveData.status) {
          this.showShaded(indexData.right, color, true);
        }
        else {
          canRemove = true;
          this.showOpponentRemove(indexData, true);
        }
      }
    }
    if(isRepeatMove && canRemove) {
      this.eventNumber = 1;
      this.repeatMove = true;
      return;
    }
    else {
      this.repeatMove = false;
    }
    this.eventNumber = 3;
  }

  showShaded(index, color, right = false) {
    if(color === 'white') {
      if (!right) {
        this.lightWhite1.setPosition(this.zoneRect[index][0], this.zoneRect[index][1]).setAlpha(0.5);
      } else {
        this.lightWhite2.setPosition(this.zoneRect[index][0], this.zoneRect[index][1]).setAlpha(0.5);
      }
    }
    else {
      if(right) {
        this.lightBlack2.setPosition(this.zoneRect[index][0], this.zoneRect[index][1]).setAlpha(0.5);
      }
      else {
        this.lightBlack1.setPosition(this.zoneRect[index][0], this.zoneRect[index][1]).setAlpha(0.5);
      }
    }
  }

  hideMoves() {
    this.lightWhite1.setAlpha(0).setData('chipToRemove', null);
    this.lightWhite2.setAlpha(0).setData('chipToRemove', null);
    this.lightBlack1.setAlpha(0).setData('chipToRemove', null);
    this.lightBlack2.setAlpha(0).setData('chipToRemove', null);
  }

  isIndexEmpty(index) {
    let status = true;
    let clr = null;
    let foundAt = null;
    Object.keys(this.state.table).forEach(color => {
      let newInd = Object.values(this.state.table[color]).indexOf(index);
      if (newInd > -1) {
        status = false;
        clr = color;
        foundAt = newInd;
      }
    });
    return {status: status, color: clr, index: index, foundAt: foundAt};
  }

  moveChip() {
    if(!this.shadedChipClicked) {
      this.eventNumber = 2;
      return false;
    }
    let chipToRemove = this.shadedChipClicked.getData('chipToRemove');
    if(chipToRemove) {
      console.log(chipToRemove);
      let chipName = chipToRemove.getData('name');
      let chipColor = chipToRemove.getData('color');
      let chipIndex = parseInt(chipName.split('_')[1]);
      chipToRemove.destroy();
      this.chips = this.chips.filter(chip => chipName);
      this.state.table[chipColor] = Object.keys(this.state.table[chipColor]).filter(index => index !== chipIndex);
    }
    this.chipClicked.setPosition(this.shadedChipClicked.x, this.shadedChipClicked.y);
    if(chipToRemove) {
      let color = this.chipClicked.getData('color');
      let direction = this.chipClicked.getData('direction');
      let name = this.chipClicked.getData('name');
      this.chipClicked = this.shadedChipClicked;
      this.chipClicked.setData({color, direction, name});
      this.zoneRect.forEach((zone, index) => {
        if(zone[0] === this.shadedChipClicked.x && zone[1] === this.shadedChipClicked.y) {
          this.chipClickedIndex = index;
        }
      });
      this.updateTable();
      this.showValidMoves(true);

    }
    if(this.repeatMove) {
      this.repeatMove = false;
      return;
    }
    this.highlight.setAlpha(0);
    this.shadedChipClicked = null;
    this.chipClickedIndex = null;
    this.hideMoves();
    this.eventNumber = 4;
  }

  ucFirst(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
  }

  showOpponentRemove(indexData, isRight = false) {
    let nextIndexData;
    if(isRight) {
      nextIndexData = this.isIndexEmpty(indexData.right)
    }
    else {
      nextIndexData = this.isIndexEmpty(indexData.left);
    }

    if(nextIndexData.status) {
      //Means the next index where the chip is moving is empty

    }
    else {
      let moveOverIndex;
      if(isRight) {
        moveOverIndex = indexData.right ? this.chipMoves[indexData.right][this.chipClicked.getData('direction')]['right'] : null;
      }
      else {
        moveOverIndex = indexData.left ? this.chipMoves[indexData.left][this.chipClicked.getData('direction')]['left'] : null;
      }
      if(!moveOverIndex) {
        return false;
      }
      //Check if the next index having different color chip
      if(nextIndexData.color !== this.currentPlayer) {
        let newPlusVar;
        if(isRight) {
          newPlusVar = indexData.right ? this.chipMoves[indexData.right][indexData.direction]['right'] : null;
        }
        else {
          newPlusVar = indexData.left ? this.chipMoves[indexData.left][indexData.direction]['left'] : null;
        }
        if(!newPlusVar) {
          return false;
        }
        const newIndexData = this.isIndexEmpty(newPlusVar);
        if(newIndexData.status) {

          let chipToRemove = this.chips.find(chip => chip.getData('name') === nextIndexData.color +'_'+nextIndexData.foundAt);
          if(indexData.color === 'white') {
            if(isRight) {
              this.lightWhite2
                .setPosition(this.zoneRect[newPlusVar][0], this.zoneRect[newPlusVar][1]).setAlpha(0.5)
                .setData({chipToRemove: chipToRemove});
            }
            else {
              this.lightWhite1.setPosition(this.zoneRect[newPlusVar][0], this.zoneRect[newPlusVar][1]).setAlpha(0.5)
                .setData({chipToRemove: chipToRemove});
            }
          }
          else {
            if(isRight) {
              this.lightBlack2.setPosition(this.zoneRect[newPlusVar][0], this.zoneRect[newPlusVar][1]).setAlpha(0.5)
                .setData({chipToRemove: chipToRemove});
            }
            else {
              this.lightBlack1.setPosition(this.zoneRect[newPlusVar][0], this.zoneRect[newPlusVar][1]).setAlpha(0.5)
                .setData({chipToRemove: chipToRemove});
            }
          }
        }
      }
    }
  }

}
