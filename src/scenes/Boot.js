export default class Boot extends Phaser.Scene {
  constructor(){
    super('Boot');
  }

  init() {}

  preload() {
    this.load.multiatlas("images", "assets/images.json", "assets");
  }

  create() {
    this.add.text(20, 20, 'Loading game...');
    this.scene.start('Game');
  }

  update() {}
}
